from docker
RUN apk add git
WORKDIR /opt/code
RUN git config --global user.email "devops-autotool@localhost"
RUN git config --global user.name "devops-autotool"

COPY bump-version /usr/bin
COPY clean-tags-and-images /usr/bin
RUN chmod +x /usr/bin/bump-version
RUN chmod +x /usr/bin/clean-tags-and-images
RUN mkdir -p /opt/code


ENTRYPOINT ["/bin/sh"]